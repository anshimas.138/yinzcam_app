import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, Subject } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GetUsersService {
  private subject = new Subject<any>();
  apiURL = 'https://api.github.com/users';
 localdata = '../assets/users.json';
  constructor(private http: HttpClient) { 
}
httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}  
getUsers(): Observable<Object>{
return this.http.get(this.localdata)
.pipe(
  retry(1),
  catchError(this.handleError)
)
}
// getRepository(data){
// return this.http.get(`https://api.github.com/users/${data}`).pipe(
//    retry(1),
//    catchError(this.handleError)
//  )
// }
localData(){
  return this.http.get('../assets/login.json')
  .pipe(
    retry(1),
    catchError(this.handleError)
  )
}
localData2(){
  return this.http.get('../assets/repo.json')
  .pipe(
    retry(1),
    catchError(this.handleError)
  )
}
localData3(){
  return this.http.get('../assets/followers.json')
  .pipe(
    retry(1),
    catchError(this.handleError)
  )
}
handleError(error) {
  let errorMessage = '';
  if(error.error instanceof ErrorEvent) {
    // Get client-side error
    errorMessage = error.error.message;
  } else {
    // Get server-side error
    errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
  }
  window.alert(errorMessage);
  return throwError(errorMessage);
}
}
