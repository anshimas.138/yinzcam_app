import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import {CommonModule} from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FilterPipe } from './pipe/filter.pipe';
import { GetUsersService } from './services/get-users.service';
import { UsersRepoComponent } from './users/users-repo.component';
import { UsersHomeComponent } from './users/users-home.component';
import { AppRoutingModule } from './/app-routing.module';
import { RepositoriesComponent } from './repositories/repositories.component';
import { FollowersComponent } from './followers/followers.component';
@NgModule({
  declarations: [
    AppComponent,
    FilterPipe,
    UsersRepoComponent,
    UsersHomeComponent,
    RepositoriesComponent,
    FollowersComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    CommonModule,
    AppRoutingModule,
  ],
  providers: [GetUsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
