import { Component, OnInit } from '@angular/core';
import {GetUsersService} from '../services/get-users.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-users-repo',
  templateUrl: './users-repo.component.html',
  styleUrls: ['./users-repo.component.css']
})
export class UsersRepoComponent implements OnInit {
data: any;
message: any;
loginData :any;
avatar: any;
name: String;
login: String;
location: String;
followers;
repos_url;
created_at;
updated_at;
avatarArray;
nameArray;
locationArray;
repos_urlArray;
  constructor(public userService: GetUsersService,  private router: Router) {
   }

  ngOnInit() {
     this.getData();
   
  }
 getData(){
this.userService.localData().subscribe(data=>{
this.loginData = data;
this.avatar = this.loginData.avatar_url;
this.name  = this.loginData.name;
this.login = this.loginData.login;
this.location = this.loginData.location;
this.repos_url = this.loginData.repos_url;
this.followers = this.loginData.followers;
})
 }
 click1 = function(){
  this.router.navigate(['followers']);
}
 click2= function () {
  this.router.navigate(['repository']);
 }
}
