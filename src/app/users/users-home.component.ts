
import { Component, OnInit} from '@angular/core';
import {GetUsersService } from '../services/get-users.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-users-home',
  templateUrl: './users-home.component.html',
  styleUrls: ['./users-home.component.css']
})

export class UsersHomeComponent implements OnInit {
  users;
  userlogin;
  idArray;
  userAvatar;
  avatarArray;
  id;
  array;
  singleArray;
  users_git;
  constructor(
    public userService: GetUsersService,
    private router: Router){
  }
  ngOnInit() {
    this.loadUsers();
  }

  loadUsers() {
    this.users = [];
    this.idArray = [];
    this.avatarArray = [];
    this.array = [];
    this.singleArray = [];
    this.userService.getUsers().subscribe(data => {
      this.users = data;
     for(let i=0;i<this.users.length;i++){
      this.userlogin = this.users[i].login;
      this.userAvatar = this.users[i].avatar_url;
      this.idArray.push(this.userlogin);
      this.avatarArray.push(this.userAvatar);
       //this.userService.getRepository(this.userlogin).subscribe(data_new=>{
      this.userService.localData().subscribe(data_new=>{
        this.users_git = data_new;
      }); 
    }
     for (var i = 0; i < this.users.length; i++) {
      this.singleArray.push({
                           loginid: this.idArray[i],
                           image: this.avatarArray[i] 
                          });
  } 
    });
  }
  btnClick= function () {
    this.router.navigate(['usersrepo']);
};
}