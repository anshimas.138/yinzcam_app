import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersRepoComponent } from './users-repo.component';

describe('UsersComponent', () => {
  let component: UsersRepoComponent;
  let fixture: ComponentFixture<UsersRepoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersRepoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersRepoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
