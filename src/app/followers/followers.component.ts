import { Component, OnInit } from '@angular/core';
import { GetUsersService } from '../services/get-users.service';
@Component({
  selector: 'app-followers',
  templateUrl: './followers.component.html',
  styleUrls: ['./followers.component.css']
})
export class FollowersComponent implements OnInit {
loginArray;
data;
login;
avatar;
avatarArray;
singleArray;
  constructor(public userService: GetUsersService) { }

  ngOnInit() {
    this.getData();
  }
  getData(){
    this.loginArray = [];
    this.avatarArray = [];
    this.singleArray = [];
    this.userService.localData3().subscribe(data=>{
    this.data = data;
    for(var i=0;i<this.data.length;i++){
      this.login = this.data[i].login;
      this.loginArray.push(this.login);
     this.avatar = this.data[i].avatar_url;
     this.avatarArray.push(this.avatar);
      this.singleArray.push({
                           loginid: this.loginArray[i],
                           image: this.avatarArray[i] 
                          });
  } 
  });
  }
}
