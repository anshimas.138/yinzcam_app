import { NgModule } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';
import { UsersRepoComponent } from './users/users-repo.component';
import { UsersHomeComponent } from './users/users-home.component';
import { RepositoriesComponent } from './repositories/repositories.component';
import { FollowersComponent } from './followers/followers.component';

const appRoutes: Routes = [
  {path: 'usershome', component: UsersHomeComponent},
  {path: 'usersrepo', component: UsersRepoComponent},
  {path: 'repository', component: RepositoriesComponent},
  {path: 'followers', component: FollowersComponent},
  {path:'', redirectTo: '/usershome', pathMatch: 'full'},
];
@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

