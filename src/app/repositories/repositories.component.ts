import { Component, OnInit } from '@angular/core';
import { GetUsersService } from '../services/get-users.service';
@Component({
  selector: 'app-repositories',
  templateUrl: './repositories.component.html',
  styleUrls: ['./repositories.component.css']
})
export class RepositoriesComponent implements OnInit {
data;
git_url;
urlArray;
  constructor(public userService: GetUsersService) { 
  }

  ngOnInit() {
    this.getData();
  }
getData(){
  this.urlArray = [];
  this.userService.localData2().subscribe(data=>{
  this.data = data;
  for(var i=0;i<this.data.length;i++){
    this.git_url = this.data[i].git_url;
    this.urlArray.push(this.git_url);
  }
})
}
}
